#include "cFolder.h"
#include <windows.h>
#include <fstream>

const std::string cFolder::cFlag = "scene.";

cFolder* cFolder::getInstance() {
	static cFolder* s_folder = new cFolder();
	return s_folder;
}

cFolder::cFolder() {
	sStrs sec;
	_maps.insert(std::make_pair(eMapType::wm, sec));
	_maps.insert(std::make_pair(eMapType::jd, sec));
	_maps.insert(std::make_pair(eMapType::iter, sec));
	_smaps = _maps;

	sUIs uis;
	uis.insert(std::make_pair(euiType::jd, sec));
	uis.insert(std::make_pair(euiType::gy, sec));
	uis.insert(std::make_pair(euiType::sj, sec));
	uis.insert(std::make_pair(euiType::yz, sec));

	_wdfs.insert(std::make_pair(eIterType::wm, uis));
	_wdfs.insert(std::make_pair(eIterType::jd, uis));
	_wdfs.insert(std::make_pair(eIterType::iter, uis));
}


void cFolder::init() {
	std::string line;
	_sceneOri = "ori";
	_sceneWm = "wm";
	_sceneJd = "jd";
	_sceneIter = "it";
	_wdf = "wd";
	_update = "upd";

	_defaults.push_back("baby");
	_defaults.push_back("shape");
	_defaults.push_back("shape.wd2");

	_uijd = "uijd";
	_wdfWm = "shape.wd0";
	_wdfIter = "shape.wd1";

	_smapWm = "smap.wd0";
	_smapJd = "smap";
	_smapIter = "smap.wd1";

	_uigy = "uigy";
	_uisj = "uisj";
	_uiyz = "uiyz";

	_music = "music";
	_sound = "sound";
	_color = "color";
}


std::string cFolder::getPath(int cut) {
	const int length = 1024; // MAX_PATH
	char arr[length];
	memset(arr, 0, length);
	GetModuleFileName(nullptr, arr, length);
	int i = -1;
	while (arr[++i] != 0) {
		if (arr[i] == '\\') {
			arr[i] = '/';
		}
	}
	if (cut == 0) {
		return arr;
	}
	int pos = 0;
	while (true) {
		// 倒数第二个/
		if (arr[--i] == '/' && (++pos) == cut) {
			arr[i + 1] = 0;
			break;
		}
	}
	return arr;
}




void cFolder::searchRoots(sStrs& roots) {
	std::string root;
	for (unsigned char c = 'd'; c <= 'z'; ++c) {
		root = c;
		roots.push_back(root + ":/");
	}
	roots.push_back("c:/");
}


void cFolder::setFront(const sStrs& filenames) {
	_fronts = filenames;
}

void cFolder::setFrontSmap(const sStrs& filenames) {
	_frontSmaps = filenames;
}


static void readSuffix(std::string& str) {
	if (str.find(".") == std::string::npos) {
		str += ".wdf";
	}
}

void cFolder::apply() {
	std::string line = "/";
	std::string front = "";
	_sceneOri = front + cFlag + _sceneOri + line;
	_sceneWm = front + cFlag + _sceneWm + line;
	_sceneJd = front + cFlag + _sceneJd + line;
	_sceneIter = front + cFlag + _sceneIter + line;
	_wdf = front + cFlag + _wdf + line;
	_update = front + cFlag + _update + line;

	for (int k = _defaults.size() - 1; k >= 0; --k) {
		readSuffix(_defaults.at(k));
	}

	readSuffix(_uijd);
	readSuffix(_wdfWm);
	readSuffix(_wdfIter);

	readSuffix(_smapWm);
	readSuffix(_smapJd);
	readSuffix(_smapIter);

	readSuffix(_uigy);
	readSuffix(_uisj);
	readSuffix(_uiyz);

	readSuffix(_music);
	readSuffix(_sound);
	readSuffix(_color);

	_color = front + _wdf + _color;
	_music = front + _wdf + _music;
	_sound = front + _wdf + _sound;


	auto& mapJd = _maps.at(eMapType::jd);
	mapJd.push_back(_sceneJd);
	mapJd.push_back(_sceneOri);
	mapJd.push_back(_sceneIter);

	auto& mapWm = _maps.at(eMapType::wm);
	mapWm.push_back(_sceneWm);
	mapWm.push_back(_sceneOri);
	mapWm.push_back(_sceneJd);
	mapWm.push_back(_sceneIter);

	auto& mapIter = _maps.at(eMapType::iter);
	mapIter.push_back(_sceneIter);
	mapIter.push_back(_sceneOri);
	mapIter.push_back(_sceneJd);

	auto& smap2 = _smaps.at(eMapType::jd);
	smap2.push_back(_smapJd);
	smap2.push_back(_smapIter);

	auto& smapWm2 = _smaps.at(eMapType::wm);
	smapWm2.push_back(_smapWm);
	smapWm2.push_back(_smapJd);
	smapWm2.push_back(_smapIter);

	auto& smapIter2 = _smaps.at(eMapType::iter);
	smapIter2.push_back(_smapIter);
	smapIter2.push_back(_smapJd);
	for (auto& it : _smaps) {
		for (auto& it2 : it.second) {
			it2 = _wdf + it2;
		}
		for (auto& it2 = _frontSmaps.rbegin(); it2 != _frontSmaps.rend(); ++it2) {
			it.second.insert(it.second.begin(), *it2);
		}
	}

	////////////////////////////////////////////////////////////////////////// 经典
	auto& wnors = _wdfs.at(eIterType::jd);
	for (auto& it : wnors) {
		auto& uis = it.second;
		for (const auto& fr : _fronts) {
			uis.push_back(fr);
		}
		for (int k = 0, size = _defaults.size(); k < size; ++k) {
			uis.push_back(_wdf + _defaults.at(k));
		}
		switch (it.first) {
		case euiType::gy:
			uis.push_back(_wdf + _uigy);
			break;
		case euiType::sj:
			it.second.push_back(_wdf + _uisj);
			break;
		case euiType::yz:
			uis.push_back(_wdf + _uiyz);
			break;
		default:
			break;
		}
		uis.push_back(_wdf + _uijd);
	}
	////////////////////////////////////////////////////////////////////////// 唯美
	auto& wwms = _wdfs.at(eIterType::wm);
	wwms = wnors;
	for (auto& it : wwms) {
		it.second.insert(it.second.begin() + _fronts.size(), _wdf + _wdfWm);
	}
	////////////////////////////////////////////////////////////////////////// 迭代
	auto& witers = _wdfs.at(eIterType::iter);
	witers = wnors;
	for (auto& it : witers) {
		it.second.insert(it.second.begin() + _fronts.size(), _wdf + _wdfIter);
	}
}