# libxiaocao

浪西游 小草终结版

# libai
战斗AI

# libass

bass音频库 (http://www.un4seen.com/)

# libitem

物品系统

# libjpeg

jpeg解码库(by云风 Cloud Wu)

# liblxy5

A星寻路 渲染画布 染色系统 丰富文体 战斗数据 鼠键输入 地图处理 音频处理 属性点 WAS类 WDF类

# libobj

角色类 房屋系统 NPC 宠物数据 图像类 精灵类 坐骑类

# libskill

技能系统

# libtrigger

任务触发器

# libtxt5 

表情 足迹 物品表 家具表 地图表 门派 模型表 召唤兽表 角色表 NPC表 脚本 商店表 技能表 武器表 坐骑祥瑞 资质表

# libui

游戏界面

# libzhuxian

主线任务系统

# 示意图

![team](https://gitee.com/ulxy/libxiaocao/raw/master/team.png)

![battle](https://gitee.com/ulxy/libxiaocao/raw/master/battle.png)

# 开发环境

Visual Studio Community 2017

![installer](https://gitee.com/ulxy/diagram_images/raw/master/installer.png)
