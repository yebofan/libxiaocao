#include "_global.h"
#include "_txt.h"


void CMainState::initGame()
{
	if (0)
	{
		// ItemManager_AutoProperty  685  997 1005
		// uiSys 1300
		vector<int> fabaos =
		{
		//	eFaBao2罗汉珠, eFaBao3赤焰,  // 这两留给69就行
			eFaBao8乾坤玄火塔, /*eFaBao17周杰伦手办, */eFaBao29镇海珠, eFaBao30五火神焰印,
		//	eFaBao21碧玉葫芦, eFaBao22神木宝鼎, eFaBao27九幽, eFaBao32慈悲,
		};
		vector<int> skis =
		{
		//	ePS_高级法术抵抗, ePS_高级幸运,   // 这两留给69就行
			ePS_高级冥思, ePS_高级魔之心, 
			ePS_须弥真言, ePS_高级法术暴击,
		};
		vector<int> skills =
		{
			ePS_高级反击/*, ePS_高级反震*/, ePS_高级吸血, ePS_高级连击,
			ePS_高级夜战, ePS_高级隐身/*, ePS_高级感知*/, ePS_高级再生, 
			ePS_高级冥思, ePS_高级必杀, ePS_高级幸运, ePS_高级神迹, 
			ePS_高级招架/*, ePS_高级永恒*/, ePS_高级敏捷, ePS_高级强力, 
			ePS_高级防御, ePS_高级偷袭, /*ePS_高级毒,*/ ePS_高级驱鬼, 
			ePS_高级神佑复生, ePS_高级否定信仰,/* ePS_高级雷属性, ePS_高级土属性, */
			/*ePS_高级火属性, ePS_高级水属性, ePS_移花接木, ePS_风刃, */
			ePS_嗜血追击, /*ePS_龙神守护, */ePS_高级法术抵抗, /*ePS_风华武圣, */
			/*ePS_法术陷井, ePS_法术防御, */ePS_苍鸾怒击, ePS_灵山禅语, 
			ePS_千钧一怒, ePS_大快朵颐
		};
		sAddPoint ap;
		ap.Set(5, 0, 0, 0, 0);

		LoadCharacterData(eJuSe虎头怪);
		LoadCharacterData(eJuSe羽灵神);
		LoadCharacterData(eJuSe逍遥生);
		LoadCharacterData(eJuSe龙太子);
		LoadCharacterData(eJuSe剑侠客);
		vector<int> mps = { eMP_魔王寨, eMP_天宫, eMP_化生寺, eMP_东海龙宫, eMP_神木林 };
		vector<int> weapons = { eWeaponType斧, eWeaponType杖, eWeaponType扇, eWeaponType枪, eWeaponType剑 };
		forr(m_Friend._friends, idx)
		{
			cObj* obj = m_FightWorkList[idx].m_pObj;
			cCharacter* cc = (cCharacter*)obj->m_pIndex;
			int mp = cct::getMenpai(eMP_随机);
			mp = mps[idx];
		//	cc->changeModel(getModelFromMp(mp));
			cPcData& pc = cc->m_PcData;
			pc.m_Lv = 175; // cct::MaxRoleLv;
			pc.m_MengPaiID = mp;
			obj->SetTrueName(pc.m_Name = cc->GetData()->m_Name);
			obj->m_Name称谓.SetString(pc.m_Name);

			sAddPoint addpoint = getAddpointFromMp(pc.m_MengPaiID/*, true*/);
			addpoint.Set(0, 0, 0, 5, 0);
			pc.setPointAfterLv(addpoint, true);
			pc.autoData(true, true, false);
			pc.m_FuZhuSkillLvList[0] = pc.m_FuZhuSkillLvList[1] = pc.m_FuZhuSkillLvList[2] = cct::MaxSkillLv;
			int m = pc.m_Lv;
			pc.m_Lv = 160; // 160;
			pc.autoEquip(false, true);
			{
				auto& pro = pc.m_Equips[1].m_Property;
				if (pro[0] < 0)
				{
					pro[0] = pc.m_Lv / 10;
					pro[1] = 7;
					g_pMainState->m_ItemManage.SetUpBaoShi(pro, pro[1], pro[0], false);
				}
			}
			{
				auto& pro = pc.m_Equips[2].m_Property;
				pro[6] = 10; // 体质
				pro[8] = 11; // 魔力
			}
			{
				auto& pro = pc.m_Equips[3].m_Property;
				g_pMainState->m_ItemManage.SetUpBaoShi(pro, pro[1], -1 * pro[0], false);
				pro[0] = pc.m_Lv / 10;
				pro[1] = 7;
				pro[6] = 10; // 体质
				pro[8] = 11; // 魔力
				g_pMainState->m_ItemManage.SetUpBaoShi(pro, pro[1], pro[0], false);
			}
			{
				auto& pro = pc.m_Equips[5].m_Property;
				g_pMainState->m_ItemManage.SetUpBaoShi(pro, pro[1], -1 * pro[0], false);
				pro[0] = pc.m_Lv / 10;
				pro[1] = 1;
				g_pMainState->m_ItemManage.SetUpBaoShi(pro, pro[1], pro[0], false);
			}
			forr(pc.m_Equips, k)
			{
				if (k != 2)
				{
					pc.m_Equips[k].m_Property[15] = 147; // 102;
				}
			}
			pc.m_Equips[2].Set(weapons[idx], pc.m_Equips[2].GetID(), false);
			pc.m_Lv = m;
			pc.apply();
			pc.FillHpMp();

			pc.m_Money[0] += 1 << 24;
			pc.m_EXP += 1 << 24;
			//////////////////////////////////////////////////////////////////////////
			vector<int> spels = { 7, 9, 10, 13, 25, 48 };
			forr(spels, kk)
			{
				pc.m_Equips[kk].m_Property[12] = spels[kk];
			}
			//////////////////////////////////////////////////////////////////////////
			forv(skis, k)
			{
				pc.AddSkill(skis[k], k);
			}
			forv(fabaos, k)
			{
				pc.m_Fabaos[k].Set(24, fabaos[k]);
				pc.m_Fabaos[k].m_Num = 1;
			}
			
			for(int n = 0; n < 5; ++n)
			{
				cPet* pPet = new cPet;
				m = cct::getPetModel(pc.m_Lv - 50, pc.m_Lv);
				m = e吸血鬼;
				mp = pc.m_Lv + cct::LvGapFromRoleAndBaby;
				mp = min(mp, cct::MaxBabyLv);
				g_pMainState->AutoPet(pPet, m, mp, true);
				pPet->m_PetData.m_ZiZhi.Set(2000, 2000, 6000, 9980, 2000, 2000, 150);
				pPet->m_PetData.SetJinJie(1);
				pPet->m_PetData.setPointAfterLv(ap, false);
				pPet->m_PetData.PointRemain = 0;
				pPet->m_PetData.m_NumofBBSkill = 0;
				forv(skills, k)
				{
					if (pPet->m_PetData.m_NumofBBSkill == 24)
					{
						break;
					}
					pPet->m_PetData.AddSkill(skills[k], k);
				}

				auto& es = pPet->m_PetData.m_Equips;
				forr(es, k)
				{
					if (es[k].m_Property.size() > 15)
					{
						es[k].m_Property[15] = (rand() % 100 < 50) ? 2098/*5*/ : 2098;
					}
				}

				pc.AddPet(pPet);
			}
			SetFightPet(pc.m_IDinFightWorkList, rand() % pc.m_NumofBB);
		//	m_ZuoQi.GetZuoQi(pc, 79 + rand() % 10);
		}
		m_InterfaceTeam.update(false, m_Friend._friends.size());

		for (int i = 42; i <= 51; ++i)
		{
			if (0 == (g_GlobalValues[1] & (1 << (i - 42))))
			{
				g_GlobalValues[1] |= (1 << (i - 42));
			}
		}
//		m_Map.LoadMap(110, 190, "长寿村");
		m_Map.LoadMap(240, 260, "大唐国境");
	}
	//	for (auto i : { e地狱战神, e如意仙子, e净瓶女娲, e炎魔神, e鼠先锋 })
	//	for (auto i : { e阴阳伞, e锦毛貂精, e犀牛将军, e小魔头, e牛魔王 })
	//	for (auto i : { e镇元大仙, e李靖, e观音姐姐, e空度禅师, e超级泡泡 })
	//	for (auto i : { e镜妖, e泪妖, e琴仙, e金铙僧, e灵灯侍者})
	//  for (auto i : { e增长巡守, e雪人, e小白泽, e小象, e般若天女 })
	// 	for (auto i : { e剑灵, e龙马, e神猴, e神鸡, e土地 })
	else
	{
		LoadCharacterData(eJuSe剑侠客);
		LoadCharacterData(1 + rand() % 14);
		LoadCharacterData(eJuSe偃无师);
		LoadCharacterData(eJuSe桃夭夭);
		LoadCharacterData(eJuSe鬼潇潇);
		sAddPoint ap;
		ap.Set(0, 0, 0, 5, 0);
		sItem2 item;
		forr(m_Friend._friends, idx)
		{
			cObj* obj = m_FightWorkList[idx].m_pObj;
			cCharacter* cc = (cCharacter*)obj->m_pIndex;
			cPcData& pc = cc->m_PcData;
			//////////////////////////////////////////////////////////////////////////
			pc.m_Lv = 30;
			pc.autoEquip(false, true);
			item.Set(29, 2);
			g_pMainState->m_ItemManage.XiangQian(pc.m_Equips[0], item);
			item.Set(29, 0);
			g_pMainState->m_ItemManage.XiangQian(pc.m_Equips[4], item);
			//////////////////////////////////////////////////////////////////////////
			pc.m_Lv = 5;
			pc.m_MengPaiID = -1;
			pc.PointRemain = cct::PointRemian;
			pc.setPointAfterLv(ap, false);
			pc.apply();
			pc.FillHpMp();

			cPet* pPet = new cPet;
			int model = cct::getPetModel(0, 5, false);
			pPet->SetData(m_PetDataManager.GetPetData(model));
			cPetData& pp = pPet->m_PetData;

			pp.Load(model, true, true);
			pp.m_Lv = pc.m_Lv + cct::LvGapFromRoleAndBaby;
			pp.m_Lv = max(pp.m_Lv, pPet->GetData()->m_LvTake + cct::LvGapFromRoleAndBaby);
			pp.PointRemain = cct::PointRemian;
			pp.setPointAfterLv(g_pMainState->getAddpointFromPet(&pp, true), true);
			pp.autoEquip(false, false);
			pp.apply();
			pp.SetJinJie(1);
			pp.FillHpMp();
			pc.AddPet(pPet);
			SetFightPet(idx, rand() % pc.m_NumofBB);

			if (idx > 0)
			{
				m_Friend._friends[idx].ctrl = false;
			}
		}

		auto& money = g_pCharacter->m_PcData.m_Money[0];
		auto& exp = g_pCharacter->m_PcData.m_EXP;
		for (int n = 5; n > 0; --n)
		{
			// 50级技能和辅助技能
			for (int k = c_NumFuZhu; k >= 0; --k)
			{
				for (int i = 50; i > 0; --i)
				{
					money += GetMainSkillMoney(i);
					continue;
					exp += GetMainSkillExp(i);
				}
			}

			// 修
			for (int k = 8; k > 0; --k)
			{
				for (int i = 2; i > 0; --i)
				{
					money += GetXiuLIanExp(i) * 2000;
				}
			}

			// 等级
			for (int i = g_pCharacter->m_PcData.m_Lv + 1; i <= 40; ++i)
			{
				break;
				exp += GetCharacterExp(i);
			}
		}

		// 送钱
		money += 0xFFFFF;

		m_InterfaceTeam.update(false, 1);
		m_Map.LoadMap(20, 74, "桃源村");
		g_pHeroObj->say("#R雷黑子#R有困难,去#R建邺城#R看看吧", 0xFFF);
	}

	m_PCHead.UpDateHead();
}