#pragma once
#include "_uiinc.h"



class uiComPet : public cInterfaceFather
{
	virtual bool initShow();
	virtual bool Init();
	virtual bool Move(int x, int y);
	virtual bool Show(cCanvas* pCanvas);
	array<int, 2> _takelvs;
	array<int, 2> _idxs;
	array<cText*, 2> _tLvs;
	array<cText*, 2> _tNames;
	array<array<cWasFile*, 4>, 2> _wass;
	array<cPet, 2> _pets;
	array<cInterfacePetSkill, 2> _petSkills;
	array<bool, 2> _isBB30s;
	array<int, 2> _idx2s;
	array<cText*, 2> _tBB30s;

};



class uiComPet2 :public cInterfaceFather
{
public:
	bool Init();
	bool initShow();
	bool ProcessInputMore();
	bool Move(int x, int y);
	bool Show(cCanvas* pCanvas);
	cPet* _petLeft, *_petRight;
	bool _isCkLeft, _isCkRight;
	int _indexLeft, _indexRifgt, _crossLeft, _crossRight, _selectLeft, _selectRight;
	cInterfaceChaKan _seeCouterLeft, _seeCouterRight, _seeLeft, _seeRight;
	std::array<cWasFile, 4> _wasLeft, _wasRight;

	cWasFile m_PetSelectBack[2];
	std::array<cText, 5> _texts;

	cWasFile m_MouseOnMark;
	cWasFile m_SelectMark;
	std::array<cWasFile, 20> _heads;

	cWasFile* _btn;
};
