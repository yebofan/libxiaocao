#include "_global.h"
#include "_txt.h"


//////////////////////////////////////////////////////////////////////////
static const sPetData& getPetData(int& idx, int& id)
{
	static map<int, vector<int>> s_petMap;
	static vector<int> s_tlvs = { 5, 15, 25, 35, 45, 55, 65, 75, 85, 95, 105, 125, 135, 145, 155, 165, 175 };
	int size = s_tlvs.size();
	const auto& ps = g_pMainState->m_PetDataManager.m_PetList;
	if (s_petMap.empty())
	{
		for (const auto& e : s_tlvs)
		{
			s_petMap.insert(make_pair(e, vector<int>()));
		}
		forv(ps, i)
		{
			if (std::find(s_tlvs.begin(), s_tlvs.end(), ps[i].m_LvTake) == s_tlvs.end())
			{
				continue;
			}
			s_petMap.find(ps[i].m_LvTake)->second.push_back(ps[i].m_ID);
		}
	}

	const auto& ids = s_petMap.at(s_tlvs[idx %= size]);
	return ps[ids[id %= ids.size()]];
}

static const cPet* getPet(int& idx)
{
	const auto& ps = g_pMainState->m_InterFaceBuyPet.m_Item;
	if (ps.empty())
	{
		return nullptr;
	}
	return ps[idx %= ps.size()];
}

static int comzz(int z1, int z2)
{
	if (z1 == z2)
	{
		return z1;
	}
	int z = (z1 + z2) / 2;
	int zmin = min(z1, z2);
	int zmax = max(z1, z2);
	return z + (rand() % (zmax - z + 1)) - (rand() % (z - zmin + 1));
}

static sZiZhi comzz(const sZiZhi& z1, const sZiZhi& z2)
{
	sZiZhi z;
	z.atk = comzz(z1.atk, z2.atk);
	z.def = comzz(z1.def, z2.def);
	z.hp = comzz(z1.hp, z2.hp);
	z.mag = comzz(z1.mag, z2.mag);
	z.speed = comzz(z1.speed, z2.speed);
	z.avoid = comzz(z1.avoid, z2.avoid);
	z.grow = comzz(z1.grow, z2.grow);
	return z;
}


static cPet* comPet(cPet* p1, cPet* p2)
{
	cPet* pet = new cPet;
	auto& p = pet->m_PetData;
	for (int i = p1->m_PetData.m_NumofBBSkill - 1; i >= 0; --i)
	{
		p.AddSkill(p1->m_PetData.m_petSkills[i]->m_Id);
	}
	for (int i = p2->m_PetData.m_NumofBBSkill - 1; i >= 0; --i)
	{
		p.AddSkill(p2->m_PetData.m_petSkills[i]->m_Id);
	}

	vector<sSkill*> sks;
	for (int i = p.m_NumofBBSkill - 1; i >= 0; --i)
	{
		sks.push_back(p.m_petSkills[i]);
	}

	forr(sks, i)
	{
		int size = sks.size();
		if (rand() % 100 < (45 + size))
		{
			sks.erase(sks.begin() + (rand() % size));
		}
	}

	forr(sks, i)
	{
		p.m_petSkills[i] = sks[i];
	}
	p.m_NumofBBSkill = sks.size();
	p.m_ZiZhi = comzz(p1->m_PetData.m_ZiZhi, p2->m_PetData.m_ZiZhi);
	return pet;
}

bool uiComPet::Init()
{
	if (!cInterfaceFather::Init())
	{
		return false;
	}
	loadBack(0xA47BE3B8);

	sChild blv1(&g_pMainState->m_Button2, " ", 10, 250);
	sChild blv2(&g_pMainState->m_Button2, " ", 225, 250);
	sChild bname1(&g_pMainState->m_Button4, " ", 10 + 50, 250);
	sChild bname2(&g_pMainState->m_Button4, " ", 225 + 50, 250);

	sChild b301(&g_pMainState->m_Button4, " ", 55, 10);
	sChild b302(&g_pMainState->m_Button4, " ", 230, 10);

	blv1.tag = bname1.tag = b301.tag = 0;
	blv2.tag = bname2.tag = b302.tag = 1;
	blv1.onClick = blv2.onClick = [&](const sChild* c)
	{
		if (_isBB30s[c->tag])
		{
			++_takelvs[c->tag];
			initShow();
		}
	};
	bname1.onClick = bname2.onClick = [&](const sChild* c)
	{
		if (_isBB30s[c->tag])
		{
			++_idxs[c->tag];
		}
		else
		{
			++_idx2s[c->tag];
		}
		initShow();
	};

	b301.onClick = b302.onClick = [&](const sChild* c)
	{
		_isBB30s[c->tag] = !_isBB30s[c->tag];
		initShow();
	};


	_tLvs[0] = (cText*)blv1.ptr2;
	_tLvs[1] = (cText*)blv2.ptr2;
	_tNames[0] = (cText*)bname1.ptr2;
	_tNames[1] = (cText*)bname2.ptr2;
	_tBB30s[0] = (cText*)b301.ptr2;
	_tBB30s[1] = (cText*)b302.ptr2;
	_idxs[0] = _idxs[1] = _takelvs[0] = _takelvs[1] = _idx2s[0] = _idx2s[1] = 0;
	_isBB30s[0] = _isBB30s[1] = true;

	sChild shadow1(g_pMainState->m_Shadow.m_NowID, 85, 200);
	sChild shadow2(g_pMainState->m_Shadow.m_NowID, 270, 200);
	shadow1.checkon = shadow2.checkon = false;
	addChild(shadow1);
	addChild(shadow2);
	for (int i = 0; i < 4; ++i)
	{
		sChild was1(g_pMainState->m_Button2.m_NowID, 85, 200);
		sChild was2(g_pMainState->m_Button2.m_NowID, 270, 200);
		_wass[0][i] = (cWasFile*)was1.ptr;
		_wass[1][i] = (cWasFile*)was2.ptr;
		_wass[0][i]->SetStatic(false);
		_wass[1][i]->SetStatic(false);
		was1.checkon = was2.checkon = false;
		addChild(was1);
		addChild(was2);
	}

	// 	sChild was1(g_pMainState->m_Button2.m_NowID, 85, 200);
	// 	sChild was2(g_pMainState->m_Button2.m_NowID, 270, 200);
	// 	_wass[0] = (cWasFile*)was1.ptr;
	// 	_wass[1] = (cWasFile*)was2.ptr;
	// 	was1.checkon = was2.checkon = false;
	// 	_wass[0]->SetStatic(false);
	// 	_wass[1]->SetStatic(false);



	addChild(blv1);
	addChild(blv2);
	addChild(bname1);
	addChild(bname2);
	addChild(b301);
	addChild(b302);
	// 	addChild(was1);
	// 	addChild(was2);

	sChild btn(&g_pMainState->m_Button2, "合", 155, 250);
	btn.onClick = [&](const sChild* c)
	{
		forv(_pets, i)
		{
			if (_pets[i].GetData()->m_LvTake == 0 || _pets[i].GetData()->m_LvTake == 115)
			{
				g_pMainState->m_Tags.Add("不可以");
				return;
			}
		}
		if (_isBB30s[0] && _isBB30s[1] && _takelvs[0] == _takelvs[1] && _idxs[0] == _idxs[1])
		{
			g_pMainState->m_Tags.Add("不可以");
			return;
		}

		if (!_isBB30s[0] && !_isBB30s[1] && _idx2s[0] == _idx2s[1])
		{
			g_pMainState->m_Tags.Add("不可以");
			return;
		}

		auto& ps = g_pMainState->m_InterFaceBuyPet.m_Item;
		if (_isBB30s[0] && _isBB30s[1] && ps.size() >= 20)
		{
			g_pMainState->m_Tags.Add("仓库已满");
			return;
		}

		cPet* pet = comPet(&_pets[0], &_pets[1]);
		pet->SetData(_pets[(rand() % 100 < 50) ? 0 : 1].GetData());

		auto& p = pet->m_PetData;
		p.modeid = pet->GetData()->m_ID;
		p.m_Name = pet->GetData()->m_Name;
		p.m_Lv = 0;
		p.PointRemain = cct::PointRemian;
		p.setPointAfterLv(sAddPoint(), false);
		p.apply();
		p.FillHpMp();

		if (!_isBB30s[0] && (!_isBB30s[1]))
		{
			int imin = min(_idx2s[0], _idx2s[1]);
			int imax = max(_idx2s[0], _idx2s[1]);
			SAFE_DELETE(ps[imax]);
			ps.erase(ps.begin() + imax);

			SAFE_DELETE(ps[imin]);
			ps.erase(ps.begin() + imin);
		}
		else
		{
			forr(_pets, i)
			{
				if (!_isBB30s[i])
				{
					SAFE_DELETE(ps[_idx2s[i]]);
					ps.erase(ps.begin() + _idx2s[i]);
				}
			}
		}
		g_pMainState->m_InterFaceBuyPet.addPet(pet);
		g_pMainState->m_ChaKan.Set(pet);
		g_pMainState->m_ChaKan.OnOff(true);
		g_pMainState->m_Tags.Add("存入仓库");
		initShow();
		cct::autoSave();
	};
	addChild(btn);

	forr(_petSkills, i)
	{
		_petSkills[i].Init();
	}
	Move(g_half320 - m_Back.GetWidth() / 2, g_half240 - m_Back.GetHeight() / 2);
	return true;
}

bool uiComPet::initShow()
{
	int kkk = 0;
	if (!getPet(kkk))
	{
		_isBB30s[0] = _isBB30s[1] = true;
	}
	forr(_pets, i)
	{
		if (_isBB30s[i])
		{
			_pets[i].SetData((sPetData*)&getPetData(_takelvs[i], _idxs[i]));
			cPetData* pd = &_pets[i].m_PetData;
			pd->m_Name.clear();
			pd->Load(_pets[i].GetData()->m_ID, true, true);
			_tBB30s[i]->SetString(toString("胚子%d", _pets[i].GetData()->m_LvTake));
			//			_tNames[i]->SetString(_pets[i].m_PetData.m_Name);
		}
		else
		{
			_pets[i] = *(cPet*)getPet(_idx2s[i]);
			_tBB30s[i]->SetString(toString("仓库%d", _idx2s[i]));
			//			_tNames[i]->SetString(_pets[i].m_PetData.m_Name);
		}
		_tNames[i]->SetString(_pets[i].GetData()->m_Name);
		const auto& pd = _pets[i].GetData();
		_tLvs[i]->SetString(pd->m_LvTake);
		_petSkills[i].UpdateData(&_pets[i].m_PetData, false);


		//		_wass[i]->Load(pd->m_ModelActs[0].m_fstand);

		vector<ulong> picid;
		array<sChangeColorData*, 4> pranse;
		_pets[i].GetID(POS_STAND, picid);
		_pets[i].GetRanse(pranse);

		for (int k = 0; k < 4; ++k)
		{
			_wass[i][k]->Free();
		}

		for (int k = 0; k < picid.size(); ++k)
		{
			_wass[i][k]->Load(picid[k], pranse[k]);
		}
	}
	return true;
}

bool uiComPet::Move(int x, int y)
{
	_petSkills[0].Move(x - 207, y - 80);
	_petSkills[1].Move(x + m_Back.GetWidth(), y - 80);
	return cInterfaceFather::Move(x, y);
}

bool uiComPet::Show(cCanvas* pCanvas)
{
	forr(_petSkills, i)
	{
		_petSkills[i].Show(pCanvas);
	}
	return cInterfaceFather::Show(pCanvas);
}





//////////////////////////////////////////////////////////////////////////
bool uiComPet2::Init()
{
	// 	if (!cInterfaceFather::Init())
	// 	{
	// 		return false;
	// 	}
	loadBack(0x16829301);

	for (int i = 0; i < 2; i++)
	{
		m_PetSelectBack[i].Load(0xF151309F);
		m_PetSelectBack[i].SetStatic(true);
		m_PetSelectBack[i].SetFrame(2 - i);
		//	m_PetSelectBack[i].SetxOffset(30);
	}

	forr(_texts, k)
	{
		_texts[k].SetColor(0);
		//		_texts[k].m_hFont = g_pMainState->m_hFont[0];
	}

	m_SelectMark.Load(0x10921CA7);
	m_SelectMark.m_NeedShow = false;
	m_MouseOnMark.Load(0x6F88F494);
	m_MouseOnMark.m_NeedShow = false;

	_seeCouterLeft.Init();
	_seeCouterRight.Init();
	_seeLeft.Init();
	_seeRight.Init();
	_seeCouterLeft.m_NeedShow = _seeCouterRight.m_NeedShow = _seeLeft.m_NeedShow = _seeRight.m_NeedShow = false;

	_petLeft = _petRight = nullptr;
	_indexLeft = _indexRifgt = _crossLeft = _crossRight = _selectLeft = _selectRight = -1;

	sChild txt("右键查看属性", 18, 366);
	((cText*)txt.ptr)->SetColor(RGB(0xFF, 0x00, 0x00));
	addChild(txt);


	sChild btn(&g_pMainState->m_Button2, "合", 201, 113);
	btn.onClick = [this](const sChild* c)
	{
		auto& ps = g_pMainState->m_InterFaceBuyPet.m_Item;
		if (ps.size() == 20 && !_isCkLeft && !_isCkRight)
		{
			g_pMainState->m_Tags.Add("仓库已满");
			return;
		}
		if (_petLeft == nullptr || _petRight == nullptr || _petLeft == _petRight ||
			_petLeft->GetData()->m_LvTake == 0 || _petLeft->GetData()->m_LvTake == 115 ||
			_petRight->GetData()->m_LvTake == 0 || _petRight->GetData()->m_LvTake == 115)
		{
			g_pMainState->m_Tags.Add("不可以");
			return;
		}
		auto& pc = g_pCharacter->m_PcData;
		auto& pets = pc.m_pPets;
		if ((!_isCkLeft && _indexLeft >= pc.m_NumofBB) ||
			(!_isCkRight && _indexRifgt >= pc.m_NumofBB) ||
			(_isCkLeft && _indexLeft >= ps.size()) ||
			(_isCkRight && _indexRifgt >= ps.size()))
		{
			g_pMainState->m_Tags.Add("不可以");
			return;
		}

		cPet* pet = comPet(_petLeft, _petRight);
		pet->SetData(((rand() % 100 < 50) ? _petLeft : _petRight)->GetData());

		auto& p = pet->m_PetData;
		p.modeid = pet->GetData()->m_ID;
		p.m_Name = pet->GetData()->m_Name;
		p.m_Lv = 0;
		p.PointRemain = cct::PointRemian;
		p.setPointAfterLv(sAddPoint(), false);
		p.apply();
		p.FillHpMp();

		if (_isCkLeft == _isCkRight)
		{
			int imin = min(_indexLeft, _indexRifgt);
			int imax = max(_indexLeft, _indexRifgt);
			if (_isCkLeft)
			{
				delete ps[imin], ps[imax];
				ps.erase(ps.begin() + imax);
				ps.erase(ps.begin() + imin);
			}
			else
			{
				pc.RemovePet(imax, true);
				pc.RemovePet(imin, true);
			}
		}
		else
		{
			if (_isCkLeft)
			{
				delete ps[_indexLeft];
				ps.erase(ps.begin() + _indexLeft);
				pc.RemovePet(_isCkRight, true);
			}
			else
			{
				delete ps[_indexRifgt];
				ps.erase(ps.begin() + _indexRifgt);
				pc.RemovePet(_indexLeft, true);

			}
		}
		g_pMainState->SetFightPet(g_pMainState->m_HeroID, -1);//
		g_pMainState->m_InterFaceBuyPet.addPet(pet);
		g_pMainState->m_ChaKan.Set(pet);
		g_pMainState->m_ChaKan.OnOff(true);
		g_pMainState->m_Tags.Add("存入仓库");
		_petLeft = _petRight = nullptr;
		_indexLeft = _indexRifgt = -1;
		initShow();
		cct::autoSave();
	};
	addChild(btn);

	Move(g_half320 - m_Back.GetWidth() / 2, g_half240 - m_Back.GetHeight() / 2);

	return true;
}


static void initShow(cPet* pet, std::array<cWasFile, 4>& was)
{
	if (pet == nullptr)
	{
		forr(was, k)
		{
			was[k].Free();
		}
		return;
	}

	vector<ulong> picid;
	array<sChangeColorData*, 4> pranse;
	pet->GetID(POS_STAND, picid);
	pet->GetRanse(pranse);

	forr (picid, k)
	{
		was[k].Load(picid[k], pranse[k]);
	}
}


bool uiComPet2::initShow()
{
	if (_selectLeft >= 0)
	{
		m_PetSelectBack[1].m_NeedShow = true;
		m_PetSelectBack[1].SetXY(18 + m_Back.GetX(), 194 + 27 * _selectLeft + m_Back.GetY());
	}
	else
	{
		m_PetSelectBack[1].m_NeedShow = false;
	}
	if (_selectRight >= 0)
	{
		m_SelectMark.m_NeedShow = true;
		m_SelectMark.SetXY(176 + m_Back.GetX() + (_selectRight % 5) * 51, 162 + (_selectRight / 5) * 51 + m_Back.GetY());
	}
	else
	{
		m_SelectMark.m_NeedShow = false;
	}

	::initShow(_petLeft, _wasLeft);
	::initShow(_petRight, _wasRight);

	forr(_texts, k)
	{
		_texts[k].SetString(k < g_pCharacter->m_PcData.m_NumofBB ? g_pCharacter->m_PcData.m_pPets[k]->m_PetData.m_Name : "");
	}

	const auto& ps = g_pMainState->m_InterFaceBuyPet.m_Item;
	forr(_heads, k)
	{
		if (k < ps.size())
		{
			const auto& p = ps[k];
			_heads[k].Load(p->GetData()->m_bighead[p->m_PetData.m_JinJieNum.GetBJinJie()]);
		}
		else
		{
			_heads[k].Load(0);
		}
	}
	return true;
}

bool uiComPet2::ProcessInputMore()
{
	bool see = false;
	if (_seeCouterLeft.m_NeedShow && isOn(g_xMouse, g_yMouse, _seeCouterLeft.m_Back.m_PreShowRect))
	{
		_seeCouterLeft.ProcessInput();
		see = true;
	}
	if (_seeCouterRight.m_NeedShow && isOn(g_xMouse, g_yMouse, _seeCouterRight.m_Back.m_PreShowRect))
	{
		_seeCouterRight.ProcessInput();
		see = true;
	}
	if (_seeLeft.m_NeedShow && isOn(g_xMouse, g_yMouse, _seeLeft.m_Back.m_PreShowRect))
	{
		_seeLeft.ProcessInput();
		see = true;
	}
	if (_seeRight.m_NeedShow && isOn(g_xMouse, g_yMouse, _seeRight.m_Back.m_PreShowRect))
	{
		_seeRight.ProcessInput();
		see = true;
	}
	if (see)
	{
		return true;
	}
	bool isLeft = g_pMainState->Mouse.GetButtonState(MOUSE_LBUTTON);
	bool isRight = g_pMainState->Mouse.GetButtonState(MOUSE_RBUTTON);
	int x = m_Back.GetX(), y = m_Back.GetY(), dx = g_xMouse - x, dy = g_yMouse - y;

	_crossLeft = _crossRight = -1;
	m_PetSelectBack[0].m_NeedShow = m_MouseOnMark.m_NeedShow = false;

	if (dx > 40 && dx < 150 && dy > 50 && dy < 140)
	{
		if (isLeft)
		{
			g_pMainState->Mouse.SetLock(MOUSE_LBUTTON);
			_petLeft = nullptr;
			_indexLeft = -1;
			initShow();
		}
		else if (isRight)
		{
			if (_petLeft != nullptr)
			{
				g_pMainState->Mouse.SetLock(MOUSE_RBUTTON);
				_seeCouterLeft.Set(_petLeft);
				_seeCouterLeft.Move(g_xMouse - _seeCouterLeft.m_Back.GetWidth() / 2, g_yMouse - _seeCouterLeft.m_Back.GetHeight() / 2);
				_seeCouterLeft.m_NeedShow = true;
			}
		}
		return true;
	}
	else if (dx > 300 && dx < 4100 && dy > 50 && dy < 140)
	{
		if (isLeft)
		{
			g_pMainState->Mouse.SetLock(MOUSE_LBUTTON);
			_petRight = nullptr;
			_indexRifgt = -1;
			initShow();
		}
		else if (isRight)
		{
			if (_petRight != nullptr)
			{
				g_pMainState->Mouse.SetLock(MOUSE_RBUTTON);
				_seeCouterRight.Set(_petRight);
				_seeCouterRight.Move(g_xMouse - _seeCouterRight.m_Back.GetWidth() / 2, g_yMouse - _seeCouterRight.m_Back.GetHeight() / 2);
				_seeCouterRight.m_NeedShow = true;
			}
		}
		return true;
	}
	else if (dx > 18 && dx < 180 && dy > 194 && dy < 194 + 27 * 5)
	{
		_crossLeft = (dy - 194) / 27;
		m_PetSelectBack[0].m_NeedShow = true;
		m_PetSelectBack[0].SetXY(x + 18, y + 194 + 27 * _crossLeft);
		if (isLeft)
		{
			_selectLeft = _crossLeft;
			if (_petLeft == nullptr)
			{
				g_pMainState->Mouse.SetLock(MOUSE_LBUTTON);
				_isCkLeft = false;
				_indexLeft = _crossLeft;
				_petLeft = g_pCharacter->m_PcData.m_pPets[_indexLeft];
				initShow();
			}
			else if (_petRight == nullptr)
			{
				g_pMainState->Mouse.SetLock(MOUSE_LBUTTON);
				_isCkRight = false;
				_indexRifgt = _crossLeft;
				_petRight = g_pCharacter->m_PcData.m_pPets[_indexRifgt];
				initShow();
			}
			else
			{
				initShow();
			}
		}
		else if (isRight)
		{
			auto pet = g_pCharacter->m_PcData.m_pPets[_crossLeft];
			if (pet != nullptr)
			{
				g_pMainState->Mouse.SetLock(MOUSE_RBUTTON);
				_seeLeft.Set(pet);
				_seeLeft.Move(g_xMouse - _seeLeft.m_Back.GetWidth() / 2, g_yMouse - _seeLeft.m_Back.GetHeight() / 2);
				_seeLeft.m_NeedShow = true;
			}
		}
		return true;
	}
	if (dx > 176 && dx < 176 + 51 * 5 && dy > 162 && dy < 162 + 51 * 4)
	{
		_crossRight = ((dy - 162) / 51) * 5 + ((dx - 176) / 51);
		m_MouseOnMark.m_NeedShow = true;
		m_MouseOnMark.SetXY(176 + x + (_crossRight % 5) * 51, 162 + y + (_crossRight / 5) * 51);
		const auto& ps = g_pMainState->m_InterFaceBuyPet.m_Item;
		if (isLeft)
		{
			_selectRight = _crossRight;
			initShow();
			if (_crossRight < ps.size())
			{
				if (_petLeft == nullptr)
				{
					g_pMainState->Mouse.SetLock(MOUSE_LBUTTON);
					_isCkLeft = true;
					_indexLeft = _crossRight;
					_petLeft = ps[_indexLeft];
					initShow();
				}
				else if (_petRight == nullptr)
				{
					g_pMainState->Mouse.SetLock(MOUSE_LBUTTON);
					_isCkRight = true;
					_indexRifgt = _crossRight;
					_petRight = ps[_indexRifgt];
					initShow();
				}
			}
		}
		else if (isRight)
		{
			if (_crossRight < ps.size())
			{
				g_pMainState->Mouse.SetLock(MOUSE_RBUTTON);
				_seeRight.Set(ps[_crossRight]);
				_seeRight.Move(g_xMouse - _seeRight.m_Back.GetWidth() / 2, g_yMouse - _seeRight.m_Back.GetHeight() / 2);
				_seeRight.m_NeedShow = true;
			}
		}
		return true;
	}
	return false;
}

bool uiComPet2::Move(int x, int y)
{
	forv(_wasLeft, k)
	{
		_wasLeft[k].SetXY(x + 100, y + 130);
		_wasRight[k].SetXY(x + 350, y + 130);
	}

	m_PetSelectBack[0].SetXY(x + 18, y + 194 + 27 * _crossLeft);
	m_PetSelectBack[1].SetXY(x + 18, y + 194 + 27 * _selectLeft);
	forv(_texts, k)
	{
		_texts[k].SetXY(x + 18, y + 194 + 27 * k + 4);
	}

	m_MouseOnMark.SetXY(176 + x + (_crossRight % 5) * 51, 162 + y + (_crossRight / 5) * 51);
	m_SelectMark.SetXY(176 + x + (_selectRight % 5) * 51, 162 + y + (_selectRight / 5) * 51);
	forv(_heads, k)
	{
		_heads[k].SetXY(176 + x + (k % 5) * 51, 162 + y + (k / 5) * 51);
	}
	return cInterfaceFather::Move(x, y);
}

bool uiComPet2::Show(cCanvas* pCanvas)
{
	cInterfaceFather::Show(pCanvas);
	if (_petLeft != nullptr)
	{
		forv(_wasLeft, k)
		{
			pCanvas->Add(&_wasLeft[k], 1);
		}
	}
	if (_petRight != nullptr)
	{
		forv(_wasRight, k)
		{
			pCanvas->Add(&_wasRight[k], 1);
		}
	}

	pCanvas->Add(&m_PetSelectBack[0], 1);
	pCanvas->Add(&m_PetSelectBack[1], 1);


	forv(_texts, k)
	{
		pCanvas->Add(&_texts[k], 1);
	}
	forv(_heads, k)
	{
		pCanvas->Add(&_heads[k], 1);
	}

	pCanvas->Add(&m_MouseOnMark, 1);
	pCanvas->Add(&m_SelectMark, 1);

	if (_seeCouterLeft.m_NeedShow)
	{
		_seeCouterLeft.Show(pCanvas);
	}
	if (_seeCouterRight.m_NeedShow)
	{
		_seeCouterRight.Show(pCanvas);
	}
	if (_seeLeft.m_NeedShow)
	{
		_seeLeft.Show(pCanvas);
	}
	if (_seeRight.m_NeedShow)
	{
		_seeRight.Show(pCanvas);
	}
	return true;
}
