#pragma once
#include <string>

namespace cc
{
	void setQ(std::string& str);

	void encode(std::string& str);

	void decode(const char* data, int size);
}